import React from 'react';
import { Link } from 'react-router-dom';

const menuList = [
  { name: 'Researcher', redirectTo: '/' },
  { name: 'Tentang Kami', redirectTo: '/' },
  { name: 'Hubungi Kami', redirectTo: '/' },
];

export const Header = ({ toggleFunction }) => {
  return (
    <header className="row header-main">
      <div className="col-xs-10 col-sm-8 col-md-3">
        <img
          src="/static/assets/populix-logo-web.png"
          alt="populix"
          className="img-responsive header-img"
          style={{ maxWidth: 120 }}
        />
      </div>
      <div className="col-xs-2 col-sm-4 col-md-9" style={{ textAlign: 'center' }}>
        <div className="col-xs-12">
          <ul className="list-inline menu-items-inline">
            {Array.isArray(menuList)
              && menuList.map((menu, key) => (
                <li key={key} className="list-item">
                  <Link to={menu.redirectTo}>{menu.name}</Link>
                </li>
              ))}
            <button
              className="btn btn-primary populix-btn signin"
              type="button"
            >
              Masuk
            </button>
            <button
              className="btn btn-danger populix-btn register"
              style={{ marginLeft: 0, marginRight: 0 }}
              type="button"
            >
              Daftar
            </button>
          </ul>
        </div>

        <div className="menu-items-icon">
          <i
            className="fa fa-bars"
            style={{ fontSize: 35 }}
            onClick={toggleFunction}
          />
        </div>
      </div>
    </header>
  );
};

export const HeaderMenuOverlay = ({ toggleFunction }) => {
  return (
    <div className="col-xs-12">
      <h3
        style={{ position: 'absolute', right: 25, color: '#d6d6d6' }}
        onClick={toggleFunction}
      >
        X
      </h3>
      <ul className="menu-item-block">
        {Array.isArray(menuList)
          && menuList.map((menu, key) => (
            <li key={key} className="list-item">
              <Link to={menu.redirectTo}>{menu.name}</Link>
            </li>
          ))}
        <div className="col-xs-12 padd-zero marg-ver-base">
          <button
            className="btn btn-primary populix-btn signin marg-zero"
            type="button"
          >
            Masuk
          </button>
        </div>
        <div className="col-xs-12 padd-zero marg-ver-base">
          <button
            className="btn btn-danger populix-btn register marg-zero"
            style={{ marginLeft: 0, marginRight: 0 }}
            type="button"
          >
            Daftar
          </button>
        </div>
      </ul>
    </div>
  );
};
