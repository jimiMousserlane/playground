import React from 'react';
import Slick from 'react-slick';

const sliderSettings = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
};

const articleSection = [
  {
    headline: `Waktu luang bisa \nmenjadi uang`,
    body: `Waktu 5-10 menit tidak akan \nterasa jika bisa memberi hasil \nyang sepadan`,
    imageUrl: `/static/assets/desktop-assets/Waktu Luang.png`,
  },
  {
    headline: `Berbagai macam \nreward menantimu`,
    body: `Kamu akan mendapat reward \nberupa uang tunai atau voucher \ndi setiap studi yang kamu ambil`,
    imageUrl: `/static/assets/desktop-assets/Kado.png`,
  },
  {
    headline: `Opinimu memberi \ndampak positif`,
    body: `Perusahaan akan semakin \nmengerti mengenai apa yang \nmasyarakat butuhkan`,
    imageUrl: `/static/assets/desktop-assets/Opini.png`,
  },
];

export const ArticleComponent = () => {
  return (
    <article className="article-parent">
      <div className="container">
        <div className="col-xs-12 article-top">
          <h4 style={{ ...styles.textStyleBase }}>Kenapa Populix?</h4>
          <div className="col-xs-12 col-md-4 padd-zero">
            <h3 style={{ ...styles.textStyleBase }} className="marg-zero">
              Siapapun kamu & <br />
              apapun pekerjaanmu
            </h3>
            <br />
            <h5
              style={{
                ...styles.textStyleBase,
                position: 'relative',
                top: -5,
                fontWeight: 'lighter',
              }}
              className="marg-zero"
            >
              Tidak ada kecuali, studi yang kami sediakan bisa <br />
              dilakukan oleh siapapun
            </h5>
          </div>
          <div className="col-xs-12 col-md-8 padd-zero">
            <img
              src="/static/assets/desktop-assets/Siapapun.png"
              className="img-responsive img-article"
              alt="group-shot"
            />
          </div>
        </div>

        <div
          className="col-xs-12 col-md-10 article-bottom"
          style={{ ...styles.textStyleBase }}
        >
          <div className="no-carousel">
            {Array.isArray(articleSection)
              && articleSection.map((article, key) => (
                <div key={key} className="col-md-4 padd-zero">
                  <div className="col-xs-12 padd-zero">
                    <img
                      src={article.imageUrl}
                      alt="alt"
                      className="img-responsive"
                    />
                  </div>
                  <div className="col-xs-12 padd-zero">
                    <h4>{article.headline}</h4>
                  </div>
                  <div className="col-xs-12 padd-zero">
                    <p>{article.body}</p>
                  </div>
                </div>
              ))}
          </div>

          <div className="with-carousel">
            <Slick className="test-lah" {...sliderSettings}>
              {Array.isArray(articleSection)
                && articleSection.map((article, key) => (
                  <div key={key} className="col-xs-12 text-center">
                    <div className="col-md-4 padd-zero">
                      <div className="col-xs-12 padd-zero">
                        <img
                          src={article.imageUrl}
                          alt="alt"
                          className="img-responsive"
                          style={{ margin: '0 auto' }}
                        />
                      </div>
                      <div className="col-xs-12 padd-zero">
                        <h4>{article.headline}</h4>
                      </div>
                      <div className="col-xs-12 padd-zero">
                        <p>{article.body}</p>
                      </div>
                    </div>
                  </div>
                ))}
            </Slick>
          </div>
        </div>
      </div>
    </article>
  );
};

const styles = {
  textStyleBase: {
    color: '#ffffff',
    whiteSpace: 'pre-wrap',
  },
};
