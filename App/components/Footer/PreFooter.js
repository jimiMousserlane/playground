import React from 'react';

export const PreFooter = () => {
  return (
    <div
      className="col-xs-12 text-center"
      style={{ paddingTop: 35, paddingBottom: 35 }}
    >
      <h3 style={{ color: '#094b6b' }}>
        YUK MULAI CARI PENGHASILAN LEWAT POPULIX!
      </h3>
      <button
        type="button"
        className="btn btn-danger btn-banner"
        style={{ margin: '15px auto' }}
      >
        Gabung Sekarang
      </button>
      <h4 style={{ color: '#848484', fontWeight: 'lighter' }}>
        Masih bingung? Yuk cari tau lebih banyak tentang Populix{' '}
        <a href="#">disini</a>.
      </h4>
    </div>
  );
};
