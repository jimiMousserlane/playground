import React from 'react';

const FooterMenu = [
  { name: 'FAQ' },
  { name: 'Karir' },
  { name: 'Aturan Privasi' },
  { name: 'Syarat & Ketentuan' },
  { name: 'Kontak' },
];

export const Footer = () => {
  return (
    <div className="col-md-12">
      <div className="col-md-6">
        <p>&copy; 2018. All Rights Reserved. Populix</p>
      </div>
      <div className="col-md-6">
        <ul className="list-inline pull-right">
          {Array.isArray(FooterMenu)
            && FooterMenu.map((menu, key) => (
              <li key={key}>
                <p>{menu.name}</p>
              </li>
            ))}
        </ul>
      </div>
    </div>
  );
};

export const MobileFooter = () => {
  return (
    <div className="col-xs-12 padd-zero">
      {Array.isArray(FooterMenu)
        && FooterMenu.map((menu, key) => (
          <li key={key} className="col-xs-6" style={{ margin: '10px 0' }}>
            {menu.name}
          </li>
        ))}
    </div>
  );
};
