import React from 'react';

export const TestimonialCard = ({ userName, profession, testimonial }) => {
  return (
    <div
      className="card col-md-8"
      style={{ textAlign: 'center', margin: '0 15%' }}
    >
      <img
        className="img-circle"
        src="https://bootcamp.umn.edu/wp-content/uploads/sites/60/2018/03/placeholder-person.png"
        alt="imge"
        style={{ margin: '0 auto', position: 'relative', top: 0 }}
      />
      <h5>{userName}</h5>
      <h6>{profession}</h6>
      <p>{testimonial}</p>
    </div>
  );
};
