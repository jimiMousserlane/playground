import React from 'react';
import Slick from 'react-slick';

const partners = [
  { name: 'n26', imageUrl: '/static/assets/partners/2000px-N26_logo.svg.png' },
  {
    name: 'plannable',
    imageUrl: '/static/assets/partners/logo-planable-verde.png',
  },
  {
    name: 'silverfin',
    imageUrl:
      '/static/assets/partners/silverfin_owler_20170314_060813_original.png',
  },
  { name: 'hopper', imageUrl: '/static/assets/partners/hopper_logo_2018.png' },
  { name: 'Slp Now', imageUrl: '/static/assets/partners/SLP_Now_Logo.png' },
];

const sliderSettings = {
  dots: false,
  infinite: false,
  speed: 500,
  slidesToShow: 0,
  slidesToScroll: 0,
  settings: 'unslick',
  variableWidth: true,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 2,
        dots: true,
        infinite: false,
      },
    },
  ],
};

export const PartnerSection = () => {
  return (
    <div className="partners-main">
      {/* <div className="no-carousel container">
        <ul className="list-inline">
          {Array.isArray(partners)
            && partners.map((partner, key) => (
              <li key={key}>
                <img
                  src={partner.imageUrl}
                  alt={partner.name}
                  className="img-responsive"
                  style={{ maxWidth: 200 }}
                />
              </li>
            ))}
        </ul>
      </div> */}
      <Slick {...sliderSettings}>
        {Array.isArray(partners)
          && partners.map((partner, key) => (
            <div key={key} style={{ width: '10%' }}>
              <img
                src={partner.imageUrl}
                alt={partner.name}
                style={{ margin: '0 15px', objectFit: 'cover' }}
                className="img-responsive"
              />
            </div>
          ))}
      </Slick>
    </div>
  );
};
