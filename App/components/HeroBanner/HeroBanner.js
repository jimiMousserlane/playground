import React from 'react';

// To simulate dynamic text;
const bannerObj = {
  mainHeadline: `Jawab Pertanyaan Dan Raih Berbagai Reward`,
  childHeadline: `Pernah terpikir untuk menghasilkan uang hanya dengan menjawab berdasarkan kebiasaanmu sehari hari ?`,
  buttonText: ``,
  buttonLinkTo: ``,
};
export const HeroBanner = () => {
  return (
    <div className="main-banner">
      <div className="banner-child">
        <h1>
          {bannerObj
            && bannerObj.mainHeadline
            && bannerObj.mainHeadline.toUpperCase()}
        </h1>
        <h3>{bannerObj && bannerObj.childHeadline}</h3>
        <button type="button" className="btn btn-danger btn-banner">
          Daftarkan Saya
        </button>
      </div>
    </div>
  );
};
