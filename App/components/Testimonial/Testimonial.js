import React from 'react';
import Slick from 'react-slick';
import { TestimonialCard } from '../TestimonialCard/TestimonialCard';

const testimonialData = [
  {
    userName: 'Albert Wesker',
    profession: 'Polisi, 25 Tahun',
    testimonial: `Lorem Ipsum Dolor Sit amet uvuvuevue guadosentai bugismile wawaluyo suprimsuprim mesmo`,
  },
  {
    userName: 'Albert Wesker',
    profession: 'Polisi, 25 Tahun',
    testimonial: `Lorem Ipsum Dolor Sit amet uvuvuevue guadosentai bugismile wawaluyo suprimsuprim mesmo`,
  },
  {
    userName: 'Albert Wesker',
    profession: 'Polisi, 25 Tahun',
    testimonial: `Lorem Ipsum Dolor Sit amet uvuvuevue guadosentai bugismile wawaluyo suprimsuprim mesmo`,
  },
];

const sliderSettings = {
  dots: true,
  infinite: false,
  speed: 500,
  slidesToShow: 2,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        infinite: false,
      },
    },
  ],
};

export const Testimonial = () => {
  return (
    <div className="testimonial-parent col-xs-12" style={{ padding: 15 }}>
      <Slick {...sliderSettings}>
        {Array.isArray(testimonialData)
          && testimonialData.map((user, key) => (
            <div key={key} className="col-xs-12 col-md-4 padd-ver-base">
              <TestimonialCard {...user} />
            </div>
          ))}
      </Slick>
    </div>
  );
};
