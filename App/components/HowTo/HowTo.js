import React from 'react';

const processStep = [
  {
    paragraph: `1. Daftar dan jawablah setiap \npertanyaan pre screening`,
    imageUrl: '/static/assets/desktop-assets/Daftar dan jawab.png',
  },
  {
    paragraph: `2. Selsaikan studi yang kamu \nbisa ambil`,
    imageUrl: '/static/assets/desktop-assets/Selesaikan Studi.png',
  },
  {
    paragraph: `3. Terima reward dan tarik \nke rekeningmu`,
    imageUrl: '/static/assets/desktop-assets/Terima Reward.png',
  },
];

export const HowTo = () => {
  return (
    <div className="howto-parent container" style={{ whiteSpace: 'pre-wrap' }}>
      <h4 style={{ color: '#094b6b' }} className="headline">
        BEGINI PROSESNYA
      </h4>
      {Array.isArray(processStep)
        && processStep.map((process, key) => (
          <div key={key} className="col-xs-12 padd-zero">
            <div className="desktop">
              <div className="col-md-6 padd-zero">
                {key % 2 === 0 && <h3>{process.paragraph}</h3>}
                {key % 2 === 1 && (
                  <img
                    src={process.imageUrl}
                    alt="alternate"
                    className="img-responsive"
                    style={{ ...styles.imageStyle, margin: '0 auto' }}
                  />
                )}
              </div>
              <div className="col-md-6 padd-zero">
                {key % 2 !== 0 && <h3>{process.paragraph}</h3>}
                {key % 2 !== 1 && (
                  <img
                    src={process.imageUrl}
                    alt="alternate"
                    className="img-responsive"
                    style={{ ...styles.imageStyle }}
                  />
                )}
              </div>
            </div>

            <div className="mobile" style={{ textAlign: 'center' }}>
              <div className="col-xs-12 padd-zero">
                <h4>{process.paragraph}</h4>
              </div>
              <div className="col-xs-12 padd-zero">
                <img
                  src={process.imageUrl}
                  alt="alternate"
                  className="img-responsive"
                  style={{ maxWidth: 200, margin: '0 auto' }}
                />
              </div>
            </div>
          </div>
        ))}
    </div>
  );
};

const styles = {
  imageStyle: {
    position: 'relative',
    bottom: 100,
  },
};
