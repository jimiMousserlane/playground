import React, { Component } from 'react';
import anime from 'animejs';
import {
  Header,
  HeaderMenuOverlay,
  HeroBanner,
  PartnerSection,
  ArticleComponent,
  HowTo,
  Testimonial,
  PreFooter,
  Footer,
  MobileFooter,
} from '../../components';

class Home extends Component {
  state = {
    showMenu: false,
  };

  _toggleMenu = () => {
    this.setState((prevState) => {
      return {
        showMenu: !prevState.showMenu,
      };
    });
    anime({
      targets: '.menu-overlay-container',
      translateX: [{ value: -100, duration: 500 }, { value: 0, duration: 500 }],
    });
  };

  render() {
    const { showMenu } = this.state;
    return (
      <div className="container-fluid">
        <Header toggleFunction={this._toggleMenu} />
        {showMenu && (
          <div className="menu-overlay-container">
            <HeaderMenuOverlay toggleFunction={this._toggleMenu} />
          </div>
        )}
        <div className="row">
          <HeroBanner />
        </div>

        {/* <div className="row">
          <PartnerSection />
      </div> */}

        <div className="row">
          <ArticleComponent />
        </div>

        <div className="row" style={{ paddingTop: 25, paddingBottom: 25 }}>
          <HowTo />
        </div>

        <div className="row">
          <Testimonial />
        </div>

        <div className="row">
          <PreFooter />
        </div>

        <footer className="row footer">
          <div className="desk-footer col-md-12">
            <Footer />
          </div>
          <div className="mobile-footer">
            <MobileFooter />
          </div>
        </footer>

        <div
          className="footer-ext"
          style={{ textAlign: 'center', marginTop: 10 }}
        >
          <p>&copy; 2018. All Rights Reserved. Populix</p>
        </div>
      </div>
    );
  }
}

export default Home;
